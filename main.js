class Gallery {



   showImage() {
      let body = document.querySelector("body");
      let images = document.querySelectorAll("img");
      let lightbox = document.createElement("div");
      lightbox.classList.add("lightbox");
      var that = this;
      images.forEach(image => {
         image.onclick = function () {
            let img = document.createElement("img");
            body.appendChild(lightbox);
            img.src = image.src;
            lightbox.appendChild(img);

            let close = document.createElement("p");
            close.innerText = "Zavřít";
            close.classList.add("close");
            lightbox.appendChild(close);
            close.onclick = that.closeImage;
         }
      });
   }

   closeImage() {
      console.log("zkouška");
   }

}

const galerie = new Gallery();
galerie.showImage();